const connection = require('../models/dbConnection')
const mailConfig = require('../config/mailConfig')

const checkVerified = level => {
    return level > 0 ? true : false
}

const checkBan = level => {
    return level === -1 ? true : false
}

const processBid = (userId, price) => {
    let query =
        'UPDATE `user` SET balance = balance - ? WHERE id = ?;UPDATE `user` SET balance = balance + ? WHERE level = 2;'
    connection.query(query, [price, userId, price], (error, results) => {
        if (error) throw error
    })
}

const mailBid = userId => {
    let query = 'SELECT * FROM `user` WHERE id = ?'
    connection.query(query, [userId], (error, results) => {
        if (error) throw error
        result = JSON.parse(JSON.stringify(results))
        let sender = mailConfig.mailUser
        let receiver = result[0].email
        let mailOptions = {
            from: sender,
            to: receiver,
            subject: 'AskBid Bidded Order',
            html: `<p><b>Dear ${
                result[0].full_name
                }</b></p><p>Successful bid.</p>`
        }
        mailConfig.transporter.sendMail(mailOptions, (error, info) => {
            if (error) throw error
            console.log('Mail sent')
        })
    })
}

const createOrder = (req, res) => {
    if (checkVerified(req.body.level) && req.body.flag === 0) {
        let query =
            'INSERT INTO `order` (user_id, item_id, size, price, flag, published_date) VALUES (?, ?, ?, ?, 0, ?)'
        connection.query(
            query,
            [
                req.body.userId,
                req.body.itemId,
                req.body.size,
                req.body.price,
                new Date()
                    .toISOString()
                    .replace(/T/, ' ')
                    .replace(/\..+/, '')
            ],
            (error, results) => {
                if (error) throw error
                res.status(200).json({ status: 'success' })
            }
        )
    } else if (!checkBan(req.body.level) && req.body.flag === 1) {
        let query =
            'SELECT * FROM `user` WHERE id = ?;'
        connection.query(
            query,
            [
                req.body.userId,
            ],
            (error, results) => {
                if (error) throw error
                result = JSON.parse(JSON.stringify(results))
                if (result[0].balance >= parseFloat(req.body.price)) {
                    let query2 =
                        'INSERT INTO `order` (user_id, item_id, size, price, flag, published_date) VALUES (?, ?, ?, ?, 1, ?);'
                    connection.query(query2, [
                        req.body.userId,
                        req.body.itemId,
                        req.body.size,
                        req.body.price,
                        new Date()
                            .toISOString()
                            .replace(/T/, ' ')
                            .replace(/\..+/, '')
                    ], (error, results) => {
                        if (error) throw error
                    })
                    processBid(req.body.userId, req.body.price)
                    mailBid(req.body.userId)
                    res.status(200).json({ status: 'success' })
                }
                else res.status(200).json({ status: 'fail' })
            }
        )
    } else res.status(401).json({ status: 'fail' })
}

const getAllOrders = (req, res) => {
    let query = 'SELECT * FROM `order`'
    connection.query(query, (error, results) => {
        if (error) throw error
        result = JSON.parse(JSON.stringify(results))
        result = result.sort((a, b) =>
            a.published_date < b.published_date ? 1 : -1
        )
        res.status(200).json({ status: 'success', result: result })
    })
}

const getUserOrders = (req, res) => {
    let query = 'SELECT * FROM `order` WHERE user_id = ? AND available = 1'
    connection.query(query, [req.params.id], (error, results) => {
        if (error) throw error
        result = JSON.parse(JSON.stringify(results))
        result = result.sort((a, b) =>
            a.published_date < b.published_date ? 1 : -1
        )
        res.status(200).json({ status: 'success', result: result })
    })
}

const getItemOrders = (req, res) => {
    let query = 'SELECT * FROM `order` WHERE item_id = ? AND available = 1'
    connection.query(query, [req.params.id], (error, results) => {
        if (error) throw error
        result = JSON.parse(JSON.stringify(results))
        result = result.sort((a, b) =>
            a.published_date < b.published_date ? 1 : -1
        )
        res.status(200).json({ status: 'success', result: result })
    })
}

const getOrder = (req, res) => {
    let query = 'SELECT * FROM `order` WHERE id = ?'
    connection.query(query, [req.params.id], (error, results) => {
        if (error) throw error
        result = JSON.parse(JSON.stringify(results))
        res.status(200).json({ status: 'success', result: result })
    })
}

const updateOrder = (req, res) => {
    if (checkVerified(req.body.level)) {
        let query =
            'UPDATE `order` SET size = ?, price = ?, flag = ?, published_date = ? WHERE id = ? AND user_id = ?'
        connection.query(
            query,
            [
                req.body.size,
                req.body.price,
                req.body.flag,
                new Date()
                    .toISOString()
                    .replace(/T/, ' ')
                    .replace(/\..+/, ''),
                req.body.orderId,
                req.body.userId
            ],
            (error, results) => {
                if (error) throw error
                res.status(200).json({ status: 'success' })
            }
        )
    } else res.status(401).json({ status: 'fail' })
}

const outOfStock = (req, res) => {
    if (checkVerified(req.body.level)) {
        let query =
            'UPDATE `order` SET available = 0 WHERE id = ? AND user_id = ?'
        connection.query(
            query,
            [req.body.orderId, req.body.userId],
            (error, results) => {
                if (error) throw error
                res.status(200).json({ status: 'success' })
            }
        )
    } else res.status(401).json({ status: 'fail' })
}

const deleteOrder = (req, res) => {
    if (checkVerified(req.body.level)) {
        let query = 'DELETE FROM `order` WHERE id = ? AND user_id = ?'
        connection.query(
            query,
            [req.body.orderId, req.body.userId],
            (error, results) => {
                if (error) throw error
                res.status(200).json({ status: 'success' })
            }
        )
    } else res.status(401).json({ status: 'fail' })
}

const getAskPrice = (req, res) => {
    let query =
        'SELECT * FROM `order` WHERE item_id = ? AND flag = 0 AND available = 1 '
    connection.query(query, [req.params.id], (error, results) => {
        if (error) throw error
        result = JSON.parse(JSON.stringify(results))
        result = result.sort((a, b) => (a.price > b.price ? 1 : -1))
        res.status(200).json({ status: 'success', result: result })
    })
}

const getBidPrice = (req, res) => {
    let query =
        'SELECT * FROM `order` WHERE item_id = ? AND flag = 1 AND available = 1'
    connection.query(query, [req.params.id], (error, results) => {
        if (error) throw error
        result = JSON.parse(JSON.stringify(results))
        result = result.sort((a, b) => (a.price < b.price ? 1 : -1))
        res.status(200).json({ status: 'success', result: result })
    })
}

module.exports = {
    createOrder,
    getAllOrders,
    getUserOrders,
    getItemOrders,
    getOrder,
    updateOrder,
    outOfStock,
    deleteOrder,
    getAskPrice,
    getBidPrice
}
