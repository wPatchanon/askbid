const connection = require('../models/dbConnection')
const checkVerified = level => {
    return level == 2 ? true : false
}
const createReportUser = (req, res) => {
    if (checkVerified(req.body.level)) {
        let query =
            'INSERT INTO `reportUser` (user_id, topic, detail, report_date) VALUES (?, ?, ?, ?)'
        connection.query(
            query,
            [
                req.body.user_id,
                req.body.topic,
                req.body.detail,
                new Date()
                    .toISOString()
                    .replace(/T/, ' ')
                    .replace(/\..+/, '')
            ],
            (error, results) => {
                if (error) throw error
                res.status(200).json({ status: 'success' })
            }
        )
    } else res.status(401).json({ status: 'fail' })
}

const getAllReportUser = (req, res) => {
    let query = 'SELECT * FROM `reportUser`'
    connection.query(query, (error, results) => {
        if (error) throw error
        result = JSON.parse(JSON.stringify(results))
        result = result.sort((a, b) =>
            a.published_date < b.published_date ? 1 : -1
        )
        res.status(200).json({ status: 'success', result: result })
    })
}

module.exports = {
    createReportUser,
    getAllReportUser
}
