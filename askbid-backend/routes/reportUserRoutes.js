const { Router } = require('express')
const reportUserControllers = require('../controllers/reportUserControllers')
const router = new Router()
router.post('/create_report_user', reportUserControllers.createReportUser)
router.get('/get_all_users', reportUserControllers.getAllReportUser)
module.exports = router