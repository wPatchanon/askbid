import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import ItemCard from '../SearchPage/ItemCard';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';


import 'typeface-roboto';


const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        margin: 30,
        //backgroundColor: theme.palette.background.paper,
    },
    grid: {
        width: '90%',
        margin: 'auto',
        minHeight: '375px',
        [theme.breakpoints.down("sm")]: {
            width: '100%',
            margin: 0,
        }
    },
    gridList: {
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
        width: '100%',
    },
    gridListTile: {
        height: '100%',
        minHeight: '100%',
    },
    // title: {
    //     color: theme.palette.primary.light,
    // },

});


function GridHorizon(props) {
    const { classes, store, title } = props;

    return (
        <div className={`${title}List`}>
            <Grid container className={classes.grid}>
                <Typography variant="h6" className={classes.title}>
                    {title}
                </Typography>
                <div className={classes.root}>
                    <GridList className={classes.gridList} cellHeight={300} cols={0} spacing={16}>
                        {store.map(itemValue => (
                            <GridListTile className={classes.gridListTile} key={itemValue.id}>
                                <ItemCard item={itemValue} />
                            </GridListTile>
                        ))}
                    </GridList>
                </div>
            </Grid>
        </div>
    );
}

GridHorizon.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GridHorizon);