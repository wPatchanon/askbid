import React from "react";
import SearchResult from "./SearchResult";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FilledInput from '@material-ui/core/FilledInput';

class SearchPage extends React.Component {
  state = {
    check: { Nike: false, Adidas: false, Converse: false, Vans: false },
    price: "",
    open: false
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };
  handleChangeBrand = name => event => {
    const tmp = event.target.checked;
    this.setState(prev => ({ check: { ...prev.check, [name]: tmp } }));
  };
  render() {
    //console.log(this.state.check)
    return (
      <div>
        <Grid container justify="center">
          <Grid item >
            <Grid container direction="row" alignItems="center" justify="center">
              <Grid item>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.check["Nike"]}
                      onChange={this.handleChangeBrand("Nike")}
                      value="Nike"
                    />
                  }
                  label="Nike"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.check["Converse"]}
                      onChange={this.handleChangeBrand("Converse")}
                      value="Converse"
                    />
                  }
                  label="Converse"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.check["Vans"]}
                      onChange={this.handleChangeBrand("Vans")}
                      value="Vans"
                    />
                  }
                  label="Vans"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.check["Adidas"]}
                      onChange={this.handleChangeBrand("Adidas")}
                      value="Adidas"
                    />
                  }
                  label="Adidas"
                />
              </Grid>
              <Grid item>
                <form autoComplete="off">
                  <FormControl variant="filled" style={{ minWidth: 120 }}>
                    <InputLabel htmlFor="demo-controlled-open-select">
                      Price
                </InputLabel>
                    <Select
                      open={this.state.open}
                      onClose={this.handleClose}
                      onOpen={this.handleOpen}
                      value={this.state.price}
                      onChange={this.handleChange}
                      input={
                        <FilledInput
                          name="price"
                          id="outlined-age-simple"
                        />
                      }
                    >
                      <MenuItem value="">
                        <em>None</em>
                      </MenuItem>
                      <MenuItem value={0}>0-999</MenuItem>
                      <MenuItem value={1000}>1000-1999</MenuItem>
                      <MenuItem value={2000}>2000-2999</MenuItem>
                      <MenuItem value={3000}>3000-3999</MenuItem>
                      <MenuItem value={4000}>4000-4999</MenuItem>
                      <MenuItem value={5000}>5000-5999</MenuItem>
                      <MenuItem value={6000}>6000-6999</MenuItem>
                      <MenuItem value={7000}>7000-7999</MenuItem>
                      <MenuItem value={8000}>8000-8999</MenuItem>
                      <MenuItem value={9000}>9000-9999</MenuItem>
                    </Select>
                  </FormControl>
                </form>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <SearchResult check={this.state.check} price={this.state.price} />
      </div>
    );
  }
}

export { SearchPage };
