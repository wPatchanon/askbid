import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import ItemCard from "./ItemCard";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import { search } from "../_actions/search.action";

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: "auto",
    maxWidth: "100%"
    //margin: '10px 2% auto 2%'
  },

  control: {
    padding: theme.spacing.unit * 2
  }
});

function SearchResult(props) {
  const { searchKey, searchResult, isLoading } = props;
  const { classes } = props;
  const [isMount, setMount] = useState(false);
  useEffect(() => {
    props.dispatch(search(searchKey));
    setMount(true);
  }, [searchKey]);

  const result = searchResult.map(itemValue => (
    <Grid key={itemValue.id} item>
      <ItemCard item={itemValue} brand={props.check} price={props.price} />
    </Grid>
  ));

  let showRes = (
    <Grid container className={classes.root} spacing={8} justify="center">
      {result}
    </Grid>
  );

  if (isMount && result.length === 0)
    showRes = <Typography variant="h4">Sorry, cannot found.</Typography>;

  return (
    <div className="search_Grid" style={{ marginTop: "60px" }}>
      {!isLoading ? showRes : <h2>Loading...</h2>}
    </div>
  );

  //SearchResult.propTypes = {
  // classes: PropTypes.object.isRequired,
  //};
}

const mapStateToProps = state => ({
  searchKey: state.search.searchKey,
  searchResult: state.search.searchResult,
  isLoading: state.search.isLoading
});

export default connect(mapStateToProps)(withStyles(styles)(SearchResult));
