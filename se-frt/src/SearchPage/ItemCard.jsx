import React, { useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";

import Card from "@material-ui/core/Card";

import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";

import Typography from "@material-ui/core/Typography";

import { CardActionArea } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import "typeface-roboto";
import Axios from "axios";
import NumberFormat from 'react-number-format';

const styles = theme => ({
    card: {
        minWidth: 250,
        minHeight: 320,
        maxWidth: 250,
        maxHeight: 320
    },
    media: {
        height: 0,
        paddingTop: "56.25%" // 16:9
    }
});

function ItemCard(props) {
    const { classes } = props;
    const { item } = props;
    const [price, setPrice] = React.useState("N/A");
    //function handleExpandClick() {
    //  setExpanded(!expanded);
    //}
    useEffect(() => {
        Axios.get(
            `${process.env.REACT_APP_DEV_API_URL}/order/get_ask_price/${props.item.id}`
        ).then(res => {
            res.data.result.length && setPrice(res.data.result[0].price);
        });
    }, []);

    function handleClick() {
        props.history.push({
            pathname: `/product/${props.item.id}`
        });
    }
    const imgUrl = `${process.env.REACT_APP_DEV_API_URL}/images/${props.item.image}`;
    const card = (<Card className={classes.card} >
        <CardActionArea onClick={handleClick} disableRipple style={{ backgroundColor: '#fafafa' }}>
            <CardMedia className={classes.media} image={imgUrl} />
            <CardContent >
                <Typography variant="h6" >
                    {props.item.brand}
                </Typography>
                <Typography variant="subtitle1" style={{ height: "3.5em" }}>
                    {props.item.desc}
                </Typography>
                <Typography variant="subtitle2" color="secondary">Lowest Ask</Typography>

                {price === 'N/A' ? <Typography variant="h6" >N/A</Typography> : <NumberFormat
                    thousandSeparator={true}
                    value={price}
                    displayType={'text'}
                    renderText={value => <Typography variant="h6" color="inherit">{value}</Typography>}
                />
                }
            </CardContent>
        </CardActionArea>
    </Card>)
    // if (props.brand === undefined) {
    //     return <p>hhhh</p>
    // } else {
    //     let brands = [];
    //     for (var key in props.brand[0]) {
    //         if (props.brand[0][key]) {
    //             brands.push(key);
    //         }
    //     }
    //     if (props.brands.length === 0 && props.price === "") {
    //         return card
    //     } else if (props.price === "") {
    //         return props.brands.indexOf(props.item.brand) > -1 ? card : null
    //     } else if (props.brands.length == 0) {
    //         return (props.price <= price && props.price + 999 >= price) ? card : null
    //     } else {
    //         return props.brands.indexOf(props.item.brand) > -1 &&
    //             (props.price <= price && props.price + 999 >= price) ? card : null
    //     }
    // }
    return !props.brand || ((!props.brand.Nike &&
        !props.brand.Adidas &&
        !props.brand.Vans &&
        !props.brand.Converse) ||
        (props.brand[props.item.brand.trim()])) &&
        ((props.price <= price && props.price + 999 >= price) || props.price === "") ? card : null


}

export default withStyles(styles)(withRouter(ItemCard));
