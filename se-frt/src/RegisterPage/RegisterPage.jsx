import React from "react";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";
import { userActions } from "../_actions";
import Button from "@material-ui/core/Button";
import { Typography } from "@material-ui/core";
import { alertActions } from "../_actions";
import Grid from "@material-ui/core/Grid";
const styles = {
  button: {
    marginTop: "20px"
  },
  buttonBlue: {
    background: "linear-gradient(45deg, #2196f3 30%, #21cbf3 90%)",
    boxShadow: "0 3px 5px 2px rgba(33, 203, 243, .30)"
  },
  formcontrol: {
    marginTop: "10px"
  },
  formControl: {
    margin: 10
  }
};
const telRegx = /^[0-9]*$/;
const emailRegex = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
class RegisterPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        cardNo: "",
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirm_password: ""
      },
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.props.dispatch(alertActions.clear());
    const { name, value } = event.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [name]: value
      }
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    this.setState({ submitted: true });
    const { user } = this.state;
    const { dispatch } = this.props;
    if (user.firstName && user.lastName && user.email && user.password) {
      dispatch(userActions.register(user));
    }
  }
  render() {
    const { registering, alert } = this.props;
    const { user, submitted } = this.state;
    return (
      <div className="col-md-6 col-md-offset-3">
        <Typography variant="h4"> Register</Typography>
        <form onSubmit={this.handleSubmit}>
          <Typography variant="h5" color="error">
            {" "}
            {alert.message}
          </Typography>
          <Grid container spacing={24}>
            <Grid item xs={6}>
              <div
                className={
                  "form-group" + (submitted && !user.cardNo ? " has-error" : "")
                }
              >
                <TextField
                  type="text"
                  className="form-control"
                  style={{ ...styles.formcontrol }}
                  label="Identification No"
                  name="cardNo"
                  value={user.cardNo}
                  error={
                    !user.cardNo.match(telRegx) ||
                    !user.cardNo ||
                    user.cardNo.length !== 13
                  }
                  placeholder="190xxxxxxxxxx"
                  onChange={this.handleChange}
                  helperText={
                    user.cardNo === "" ? "Identification No is empty!" : " "
                  }
                />
                {submitted && !user.cardNo && (
                  <div className="help-block">
                    Identification No is required
                  </div>
                )}
              </div>
            </Grid>
            <Grid item xs={6}>
              <div
                className={
                  "form-group" + (submitted && !user.email ? " has-error" : "")
                }
              >
                <TextField
                  type="email"
                  error={!user.email.match(emailRegex)}
                  className="form-control"
                  style={{ ...styles.formcontrol }}
                  label="E-mail"
                  name="email"
                  value={user.email}
                  placeholder="Noey@BCP48.com"
                  onChange={this.handleChange}
                  helperText={user.email === "" ? "Email is empty!" : " "}
                />
                {submitted && !user.email && (
                  <div className="help-block">email is required</div>
                )}
              </div>
            </Grid>
            <Grid item xs={6}>
              <div
                className={
                  "form-group" +
                  (submitted && !user.firstName ? " has-error" : "")
                }
              >
                <TextField
                  type="text"
                  className="form-control"
                  style={{ ...styles.formcontrol }}
                  label="First Name"
                  name="firstName"
                  value={user.firstName}
                  error={!user.firstName}
                  placeholder="Kanteera"
                  onChange={this.handleChange}
                  helperText={
                    user.firstName === "" ? "First Name is empty!" : " "
                  }
                />
                {submitted && !user.firstName && (
                  <div className="help-block">First Name is required</div>
                )}
              </div>
            </Grid>
            <Grid item xs={6}>
              <div
                className={
                  "form-group" +
                  (submitted && !user.lastName ? " has-error" : "")
                }
              >
                <TextField
                  type="text"
                  className="form-control"
                  style={{ ...styles.formcontrol }}
                  label="Last Name"
                  name="lastName"
                  placeholder="Wadcharathadsanakul"
                  value={user.lastName}
                  error={!user.lastName}
                  onChange={this.handleChange}
                  helperText={
                    user.lastName === "" ? "Last Name is empty!" : " "
                  }
                />
                {submitted && !user.lastName && (
                  <div className="help-block">Last Name is required</div>
                )}
              </div>
            </Grid>
            <Grid item xs={6}>
              <div
                className={
                  "form-group" +
                  (submitted && !user.password ? " has-error" : "")
                }
              >
                <TextField
                  type="password"
                  className="form-control"
                  style={{ ...styles.formcontrol }}
                  label="Password"
                  name="password"
                  value={user.password}
                  placeholder="Between 6 to 20 characters..."
                  error={user.password.length < 6 || user.password.length > 20}
                  onChange={this.handleChange}
                  helperText={user.password === "" ? "Password is empty!" : " "}
                />
                {submitted && !user.password && (
                  <div className="help-block">Password is required</div>
                )}
              </div>
            </Grid>
            <Grid item xs={6}>
              <TextField
                mx="auto"
                type="password"
                className="form-control"
                style={{ ...styles.formcontrol }}
                label="Confirm Password"
                name="confirm_password"
                value={user.confirm_password}
                placeholder="Between 6 to 20 characters..."
                error={
                  user.password.length < 6 ||
                  user.password.length > 20 ||
                  user.password != user.confirm_password
                }
                onChange={this.handleChange}
                helperText={
                  user.confirm_password === ""
                    ? "Does not match with the password"
                    : " "
                }
              />
            </Grid>
            <Grid item xs={12}>
              <div className="form-group">
                <Button
                  type="submit"
                  variant="contained"
                  color="secondary"
                  onClick={this.handleSubmit}
                  disabled={
                    user.password.length < 6 ||
                    user.password.length > 20 ||
                    user.password != user.confirm_password ||
                    !user.email.match(emailRegex) ||
                    !user.lastName ||
                    !user.firstName ||
                    !user.cardNo
                  }
                  style={{
                    ...styles.button,
                    ...(this.state.color === "blue" ? styles.buttonBlue : {})
                  }}
                >
                  Register
                </Button>
                {registering && (
                  <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                )}
              </div>
            </Grid>
          </Grid>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  const { registering } = state.registration;
  return {
    registering,
    alert
  };
}

const connectedRegisterPage = connect(mapStateToProps)(RegisterPage);
export { connectedRegisterPage as RegisterPage };
