import { userConstants } from '../_constants';
import { userService } from '../_services';

export function search(keyword) {
    return dispatch => {
        dispatch(request())

        userService.search(keyword)
            .then(
                result => {
                    dispatch(success(result.result))
                },
                error => { }
            );
    };
    function request() { return { type: userConstants.SEARCH_REQUEST } }
    function success(result) { return { type: userConstants.SEARCH_SUCESS, result } }
}