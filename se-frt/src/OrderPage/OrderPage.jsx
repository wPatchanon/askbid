import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from "@material-ui/core/styles";
import axios from 'axios'

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    head: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
    root: {
        width: "90%",
        margin: 'auto',
        overflowX: 'auto',
    },
    table: {
    },
});

class OrderPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: []
        };
    }

    componentDidMount() {
        const { user, userauth } = this.props
        if (user) {
            axios.post(`${process.env.REACT_APP_DEV_API_URL}/match/get_user_unmatch`,
                {
                    userId: user.userId,
                    level: user.level
                },
                {
                    headers: {
                        "Authorization": `Bearer ${userauth.token}`
                    }
                })
                .then(res => this.setState({ orders: res.data.result }))
        }
    }

    render() {
        const { classes } = this.props;
        const { orders } = this.state;
        console.log(orders)

        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell className={classes.head}>ID</TableCell>
                            <TableCell className={classes.head} align="center">Date</TableCell>
                            <TableCell className={classes.head} align="center">Item</TableCell>
                            <TableCell className={classes.head} align="right">Price</TableCell>
                            <TableCell className={classes.head} align="right">Status</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {orders.map(order => (
                            <TableRow key={order.id} className={classes.row}>
                                <TableCell component="th" scope="row">
                                    {order.id}
                                </TableCell>
                                <TableCell component="th" scope="row" align="right">
                                    {order.published_date.substring(0, order.published_date.length - 5)}
                                </TableCell>
                                <TableCell component="th" scope="row">
                                    {order.item}
                                </TableCell>
                                <TableCell align="right">{order.price}</TableCell>
                                <TableCell align="right">
                                    Unmatched
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

const mapStateToProps = state => ({
    user: state.authentication.user,
    userauth: state.authentication.user
});

const connectedOrder = connect(mapStateToProps)(withStyles(styles)(OrderPage));
export { connectedOrder as OrderPage };
