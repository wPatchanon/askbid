import React from "react";
import { connect } from "react-redux";
import { PrivateRoute } from "../_components";
import { HomePage } from "../HomePage";
import { SearchPage } from "../SearchPage";
import { ProductPage } from "../ProductPage";
import { ProfilePage } from "../UserPage";
import { EditPage } from "../UserPage";
import { VerifyPage } from "../UserPage";
import { ReportPage } from "../ReportPage";
import { AdminPage } from "../AdminPage";
import { OrderPage } from "../OrderPage";
import { MatchedPage } from "../MatchedPage";
import NavBar from "./NavBar";
import { withRouter, Switch, Route } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';


const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#6d6d6d',
      main: '#424242',
      dark: '#1b1b1b',
      contrastText: '#ffffff',
    },
    secondary: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
      contrastText: '#ffffff',
    },
  },
  typography: {
    useNextVariants: true,
  },
});

class App extends React.Component {
  constructor(props) {
    super(props);
    const { dispatch } = this.props;
  }
  render() {
    const { alert } = this.props;
    return (
      <MuiThemeProvider theme={theme}>
        <div style={{ backgroundColor: "#f5f5f5", padding: 15 }}>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <NavBar />

          <div className="container" style={{ marginTop: "70px" }}>
            <div className="col-sm-8 col-sm-offset-2">

              <Switch>
                <Route exact path="/" component={HomePage} />
                <Route path="/search" component={SearchPage} />
                <Route exact path="/product/:id" component={ProductPage} />
                <Route path="/profile" component={ProfilePage} />
                <Route path="/edit" component={EditPage} />
                <Route path="/verify" component={VerifyPage} />
                <Route path="/report" component={ReportPage} />
                <Route path="/admin" component={AdminPage} />
                <Route path="/order" component={OrderPage} />
                <Route path="/match" component={MatchedPage} />
              </Switch>
            </div>
          </div>
          {/* <Typography
            variant="h6"
            color="inherit"
            noWrap
            style={{ textAlign: 'center', marginTop: 20 }}
          >
            2019 AskBid
        </Typography> */}
        </div>
      </MuiThemeProvider>
    );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  return {
    alert
  };
}
/*
      {alert.message && (
    <div className={`alert ${alert.type}`}>{alert.message}</div>
  )}
  */
const connectedApp = connect(mapStateToProps)(withRouter(App));
export { connectedApp as App };
