import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import InputBase from "@material-ui/core/InputBase";
import Badge from "@material-ui/core/Badge";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MailIcon from "@material-ui/icons/Mail";
import NotificationsIcon from "@material-ui/icons/Notifications";
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import MoreIcon from "@material-ui/icons/MoreVert";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import muitheme from "../theme/muitheme";
import "typeface-roboto";
import { userConstants } from "../_constants";
import Button from "@material-ui/core/Button";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';
import Grid from '@material-ui/core/Grid';
import { fade } from '@material-ui/core/styles/colorManipulator';
import HomeIcon from '@material-ui/icons/Home';
import { LoginPage } from "../LoginPage";
import { RegisterPage } from "../RegisterPage";
import Modal from "@material-ui/core/Modal";
import { userActions } from "../_actions";
import Axios from "axios";
import mainLogo from './logo.png';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  };
}
const styles = theme => ({
  root: {
    width: "100%"
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block"
    }
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing.unit * 3,
      width: "auto"
    }
  },
  navBar: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex"
    }
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "none",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    }
  },
  inputRoot: {
    color: "inherit",
    width: "100%"
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: 200,
      paddingLeft: theme.spacing.unit * 10,
    }
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "block"
    }
  },
  sectionMobile: {
    display: "block",
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  paper: {
    position: "absolute",
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: "none",
    textAlign: "center"
  },
  margin: {
    margin: theme.spacing.unit
  },
  icon: {
    marginRight: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
});

function NavBar(props) {
  const { classes, isLoggedIn } = props;
  const [key, setKey] = React.useState("");
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const [value, setValue] = React.useState(0);
  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const [RegBeDisabled, setRegBeDisabled] = React.useState(true);
  const [regOrLogin, setregOrLogin] = React.useState("Register");
  const [open, setOpen] = React.useState(false);
  const [norder, setNorder] = React.useState(0)
  const [nmatch, setNmatch] = React.useState(0)

  const [profile, setProfile] = React.useState();


  React.useEffect(() => {
    if (isLoggedIn) {
      const data = {
        userId: props.user.userId,
      };
      const header = {
        "Authorization": `Bearer ${props.user.token}`
      }
      Axios.post(`${process.env.REACT_APP_DEV_API_URL}/user/get_profile`, data, { headers: header })
        .then(res => {
          setProfile(res.data.result);
        }
        )

      Axios.post(`${process.env.REACT_APP_DEV_API_URL}/match/get_pending_match`, { userId: props.user.userId, level: props.user.level }, { headers: header })
        .then(res => {
          setNmatch(res.data.result.length);
        }
        )

      Axios.get(`${process.env.REACT_APP_DEV_API_URL}/order/get_user_orders/${props.user.userId}`)
        .then(res => {
          setNorder(res.data.result.length)
        })


    }
    else {
      setProfile()
    }
  }, [props.user])



  function handleProfileMenuOpen(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleMobileMenuClose() {
    setMobileMoreAnchorEl(null);
  }

  function handleMenuClose() {
    setAnchorEl(null);
    handleMobileMenuClose();
  }

  function handleMenuCloseLog() {
    //userActions.logout();
    setAnchorEl(null);
    handleMobileMenuClose();
    props.dispatch(userActions.logout());
    props.history.push("/");
  }

  function handleMobileMenuOpen(event) {
    setMobileMoreAnchorEl(event.currentTarget);
  }

  function updateKeyword(event) {
    setKey(event.target.value);
  }

  function keyPress(event) {
    if (event.keyCode === 13) {
      //console.log(key);
      setValue(1);
      props.dispatch({ type: userConstants.SET_KEYWORD, keyword: key });
      props.history.push("/search");
    }
  }

  function handleChangeTab(event, newValue) {
    setValue(newValue);
  }

  const renderForGuest = (
    <div>
      <Button
        onClick={() => {
          setOpen(!open);
        }}
        color='secondary'
      >
        Login/Register
      </Button>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={() => setOpen(!open)}
      >
        <div style={getModalStyle()} className={classes.paper}>
          <img
            src={require("../UserLoginRegPage/logo.png")}
            style={{ width: 200, height: 200 }}
          />
          {RegBeDisabled ? <LoginPage /> : <RegisterPage />}
          <Button
            size="small"
            className={classes.margin}
            onClick={() => {
              setRegBeDisabled(!RegBeDisabled);
              setregOrLogin(RegBeDisabled ? "Login" : "Register");
            }}
          >
            {regOrLogin}
          </Button>
        </div>
      </Modal>
    </div>
  );

  const renderForUser = (
    <div>
      <div className={classes.sectionDesktop}>
        <Tooltip title='Orders'>
          <IconButton color="inherit" onClick={() => props.history.push("/order")}>
            <Badge badgeContent={norder} color="secondary">
              <ShoppingCartIcon />
            </Badge>
          </IconButton>
        </Tooltip>

        <Tooltip title='Matched Orders'>
          <IconButton color="inherit" onClick={() => props.history.push("/match")}>
            <Badge badgeContent={nmatch} color="secondary">
              <AssignmentTurnedInIcon />
            </Badge>
          </IconButton>
        </Tooltip>

        <IconButton
          aria-owns={isMenuOpen ? "material-appbar" : undefined}
          aria-haspopup="true"
          onClick={handleProfileMenuOpen}
          color="inherit"
        >
          {profile && (profile.avatar_image ? <Avatar src={`${process.env.REACT_APP_DEV_API_URL}/avatars/${profile.avatar_image}`} /> :
            <Avatar style={{ backgroundColor: 'green' }}>{profile.full_name.substring(0, 1)}</Avatar>
          )}
        </IconButton>
      </div >
      <div className={classes.sectionMobile}>
        <IconButton
          aria-haspopup="true"
          onClick={handleMobileMenuOpen}
          color="inherit"
        >
          <MoreIcon />
        </IconButton>
      </div>
    </div >
  );

  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose} component={Link} to="/profile">
        My Account
      </MenuItem>
      <MenuItem onClick={handleMenuClose} component={Link} to="/order">
        My Orders
      </MenuItem>
      <MenuItem onClick={handleMenuClose} component={Link} to="/match">
        My Matched Orders
      </MenuItem>
      <MenuItem onClick={handleMenuClose} component={Link} to="/report">
        Report
      </MenuItem>
      {isLoggedIn && props.user.level === 2 && (
        <MenuItem onClick={handleMenuClose} component={Link} to="/admin">
          Admin
        </MenuItem>
      )}
      <MenuItem onClick={handleMenuCloseLog}>Log out</MenuItem>
    </Menu>
  );

  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem onClick={() => {props.history.push("/order"); handleMenuClose()}}>
        <IconButton color="inherit">
          <Badge badgeContent={norder} color="secondary">
            <ShoppingCartIcon />
          </Badge>
        </IconButton>
        <p>Order</p>
      </MenuItem>
      <MenuItem onClick={() => {props.history.push("/match"); handleMenuClose()}}>
        <IconButton color="inherit">
          <Badge badgeContent={nmatch} color="secondary">
            <AssignmentTurnedInIcon />
          </Badge>
        </IconButton>
        <p>Matched Order</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton color="inherit">
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );
  return (
    <div className={classes.root}>
      <AppBar position="fixed" color="primary" >
        <Toolbar>
          <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
            <Grid container direction="row" alignItems="center">
              <Grid item>
                <Avatar alt="Askbid" src={mainLogo} className={classes.icon} />
              </Grid>
              <Grid item>
                <Grid container direction="row" alignItems="center">
                  <Typography
                    className={classes.title}
                    variant="h5"
                    color="inherit"
                    noWrap
                  >
                    Ask
                </Typography>
                  <Typography
                    className={classes.title}
                    variant="h5"
                    color="secondary"
                    noWrap
                  >
                    Bid
                </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Link>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              defaultValue={props.searchKey}
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput
              }}
              onChange={updateKeyword}
              onKeyDown={keyPress}
            />
          </div>
          <div className={classes.grow} />
          <div className={classes.navBar}>
            <Button className={classes.button} color="secondary" onClick={() => handleChangeTab(null, 0)} component={Link} to="/" >
              <HomeIcon className={classes.leftIcon} />
              Home
            </Button>
            <Button className={classes.button} color="inherit" onClick={() => handleChangeTab(null, 1)} component={Link} to="/search">
              Product
            </Button>
            <Button className={classes.button} color="inherit" onClick={() => handleChangeTab(null, 2)} component={Link} to="/">
              About us
            </Button>
          </div>

          {isLoggedIn ? renderForUser : renderForGuest}
        </Toolbar>
      </AppBar>
      {isLoggedIn && renderMenu}
      {isLoggedIn && renderMobileMenu}
    </div>
  );
}

const mapStateToProps = state => ({
  searchKey: state.search.searchKey,
  isLoggedIn: state.authentication.loggedIn,
  user: state.authentication.user
});

export default connect(mapStateToProps)(withStyles(styles)(withRouter(NavBar)));
