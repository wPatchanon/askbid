import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { store } from './_helpers';
import { App } from './App';
import { Route, Router, Switch } from 'react-router-dom'
import { createBrowserHistory } from 'history'



require('dotenv').config()

export const history = createBrowserHistory();
render(
    <Router history={history}>
        <Provider store={store}>
            <Route path="/" component={App} />
        </Provider>
    </Router>,
    document.getElementById('app')
);