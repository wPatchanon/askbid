import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from "@material-ui/core/styles";
import axios from 'axios'

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    head: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
    root: {
        width: "90%",
        margin: 'auto',
        overflowX: 'auto',
    },
    table: {
    },
});

const cmessg = ['Wait for shipping',
    'Shipping', 'Closed'];
const smessg = ['Click if Shipped',
    'Wait for customer to receive',
    'Click to Close Order'];

class MatchedPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: []
        };
    }

    componentDidMount() {
        const { user, userauth } = this.props
        if (user) {
            axios.post(`${process.env.REACT_APP_DEV_API_URL}/match/get_pending_match`,
                {
                    userId: user.userId,
                    level: user.level
                },
                {
                    headers: {
                        "Authorization": `Bearer ${userauth.token}`
                    }
                })
                .then(res => this.setState({ orders: res.data.result }))
        }
    }

    onChangeStatus(order_id, status, cusID) {
        const { user, userauth } = this.props
        const { orders } = this.state
        if (user) {
            axios.post(`${process.env.REACT_APP_DEV_API_URL}/match/update_shipping`,
                {
                    shippingStatus: status,
                    cusId: cusID,
                    level: user.level,
                    orderId: order_id,
                },
                {
                    headers: {
                        "Authorization": `Bearer ${userauth.token}`
                    }
                }).then(() => {
                    this.setState({
                        orders: orders.map(o => {
                            if (o.order_id == order_id) {
                                o.status = status
                            }
                            return o;
                        })
                    })
                })
        }
    }
    render() {
        const { classes } = this.props;
        const { orders } = this.state;
        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell className={classes.head}> Order ID</TableCell>
                            <TableCell className={classes.head} align="center">Date</TableCell>
                            <TableCell className={classes.head} align="center">Item</TableCell>
                            <TableCell className={classes.head} align="right">Price</TableCell>
                            <TableCell className={classes.head} align="right">Status</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {orders.map(order => (
                            <TableRow key={order.order_id} className={classes.row}>
                                <TableCell component="th" scope="row">
                                    {order.order_id}
                                </TableCell>
                                <TableCell component="th" scope="row">
                                    {order.paid_date.substring(0, order.paid_date.length - 5)}
                                </TableCell>
                                <TableCell component="th" scope="row">
                                    {order.item}
                                </TableCell>
                                <TableCell align="right" component="th" scope="row">{order.price}</TableCell>
                                <TableCell align="right" component="th" scope="row">
                                    {(order.isCus && order.status <= 2) && (<div>
                                        {cmessg[order.status]}
                                        {(order.status == 1) &&
                                            (<Button variant="contained" color="primary" onClick={() => this.onChangeStatus(order.order_id, 2, order.cus_id)}>
                                                I've received
                                            </Button>)
                                        }
                                    </div>)}
                                    {(!order.isCus) && (<div>
                                        {smessg[order.status]}
                                        {(order.status == 0) &&
                                            (<Button variant="contained" color="primary" onClick={() => this.onChangeStatus(order.order_id, 1, order.cus_id)}>
                                                I've Shipped Order
                                            </Button>)
                                        }
                                        {(order.status == 2) &&
                                            (<Button variant="contained" color="primary" onClick={() => this.onChangeStatus(order.order_id, 3, order.cus_id)}>
                                                Close Order
                                            </Button>)
                                        }
                                    </div>)}
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}

const mapStateToProps = state => ({
    user: state.authentication.user,
    userauth: state.authentication.user
});

const connected = connect(mapStateToProps)(withStyles(styles)(MatchedPage));
export { connected as MatchedPage };
