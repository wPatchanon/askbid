import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import ItemCard from '../SearchPage/ItemCard'
import { withStyles } from "@material-ui/core/styles";
import Feed from "../Feed/index"

const styles = theme => ({
    root: {
        flexGrow: 1,
        margin: 'auto',
        maxWidth: '100%'
        //margin: '10px 2% auto 2%'

    },

    control: {
        padding: theme.spacing.unit * 2,
    },
});


class HomePage extends React.Component {
    handleDeleteUser(id) {
        return (e) => this.props.dispatch(userActions.delete(id));
    }

    render() {
        const { user, users, classes } = this.props;


        return (
            <div>
                <Feed />
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(withStyles(styles)(HomePage));
export { connectedHomePage as HomePage };