import React from "react";
import { connect } from "react-redux";
import { userActions } from "../_actions";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { Typography } from "@material-ui/core";
import { alertActions } from "../_actions";
const styles = {
  button: {
    marginTop: "20px"
  },
  buttonBlue: {
    background: "linear-gradient(45deg, #2196f3 30%, #21cbf3 90%)",
    boxShadow: "0 3px 5px 2px rgba(33, 203, 243, .30)"
  },
  formcontrol: {
    marginTop: "10px"
  }
};
const emailRegex = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
class LoginPage extends React.Component {
  constructor(props) {
    super(props);

    // reset login status
    // this.props.dispatch(userActions.logout());
    this.state = {
      email: "",
      password: "",
      submitted: false,
      err: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    //this.props.dispatch(alertActions.clear());
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    const { email, password } = this.state;
    const { dispatch } = this.props;
    if (email && password) {
      dispatch(userActions.login(email, password));
    }
  }

  render() {
    const { loggingIn, alert } = this.props;
    const { email, password, submitted } = this.state;
    return (
      <div >
        <div className="text-center" />
        <Typography variant='h4'>Login</Typography>
        <Typography variant='body1' color='error'> {alert.message}</Typography>
        <form name="form" onSubmit={this.handleSubmit}>
          <div
            className={"form-group" + (submitted && !email ? " has-error" : "")}
          >
            <TextField
              type="email"
              error={!email.match(emailRegex)}
              className="form-control"
              placeholder="Noey@BCP48.com"
              style={{ ...styles.formcontrol }}
              name="email"
              value={email}
              label="E-mail"
              onChange={this.handleChange}
            />

            {submitted && !email && (
              <div className="help-block">email is required</div>
            )}
          </div>
          <div
            className={
              "form-group" + (submitted && !password ? " has-error" : "")
            }
          >
            <TextField
              type="password"
              className="form-control"
              placeholder="********"
              error={password.length < 6 || password.length > 20}
              style={{ ...styles.formcontrol }}
              name="password"
              value={password}
              label="Password"
              onChange={this.handleChange}
            />
            {submitted && !password && (
              <div className="help-block">Password is required</div>
            )}
          </div>
          <Button type="submit"
            variant="contained"
            color="secondary"
            disabled={
              password.length < 6 ||
              password.length > 20 ||
              !email.match(emailRegex)
            }
            onClick={this.handleSubmit}
            style={{
              ...styles.button,
              ...(this.state.color === "blue" ? styles.buttonBlue : {})
            }}
          >
            Login
          </Button>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { loggingIn } = state.authentication;
  const { alert } = state;
  return {
    loggingIn, alert
  };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage };
