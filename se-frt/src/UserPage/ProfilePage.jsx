import React, { useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";
import axios from "axios";

const styles = theme => ({
  avatar: {
    margin: 10,
    maxWidth: 256,
    maxHeight: 256,
    width: '50vw',
    height: '50vw'
  },
  container: {
    overflowX:'auto'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
    fontSize: 17
  },
  button: {
    margin: theme.spacing.unit
  },
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  }
});

class ProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      uid: "",
      name: "",
      address: "",
      tel: "",
      cost: "",
      avatar: null,
      verified: null
    };
  }
  componentDidMount() {
    axios
      .post(
        `${process.env.REACT_APP_DEV_API_URL}/user/get_profile/`,
        { userId: this.props.user.userId },
        {
          headers: { Authorization: "Bearer " + this.props.user.token }
        }
      )
      .then(res => {
        this.setState({
          uid: res.data.result.id,
          name: res.data.result.full_name,
          address: res.data.result.address,
          tel: res.data.result.tel_no,
          cost: res.data.result.balance,
          avatar: res.data.result.avatar_image,
          verified: res.data.result.level
        });
      });
  }

  render() {
    const { classes } = this.props;
    return (
      <div style={{ display: "flex", justifyContent: "center" }}>
        <form
          className={classes.container}
          noValidate
          autoComplete="off"
          style={{ display: "inline" }}
        >
          <Paper className={classes.root} elevation={1}>
            <Grid container justify="center" alignItems="center">
              <Typography variant="h4">
                Account
              </Typography>
            </Grid>
            <br />
            <Grid container justify="center" alignItems="center">
              <Avatar
                src={this.state.avatar && `${process.env.REACT_APP_DEV_API_URL}/avatars/${this.state.avatar}`}
                className={classes.avatar}
              />
            </Grid>
            <br />
            <br />
            <Typography
              className={classes.textField}
              color="textSecondary"
              gutterBottom
              margin="normal"
            >
              UserID: {this.state.uid}
              <br />
              <br />
              Fullname: {this.state.name}
              <br />
              <br />
              Address: {this.state.address}
              <br />
              <br />
              Tel: {this.state.tel}
              <br />
              <br />
              Balance: {this.state.cost}
            </Typography>
            <Grid container justify="center" alignItems="center">
              <Button
                variant="contained"
                color="secondary"
                size="small"
                className={classes.button}
                component={Link}
                to="/edit"
              >
                Edit
              </Button>
            </Grid>
          </Paper>
          <br />
          <Grid container justify="center" alignItems="center">
            {this.state.verified == 0 && (
              <Button
                variant="outlined"
                color="secondary"
                className={classes.button}
                component={Link}
                to="/verify"
              >
                Verify to be vendee or vendor
              </Button>
            )}
            {this.state.verified >= 1 && (
              <Button
                variant="contained"
                color="secondary"
                disabled
                className={classes.button}
              >
                Your account has been verified
              </Button>
            )}
          </Grid>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}

const connectedProfilePage = connect(mapStateToProps)(
  withStyles(styles)(ProfilePage)
);
export { connectedProfilePage as ProfilePage };
