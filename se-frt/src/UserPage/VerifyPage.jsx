import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import axios from "axios";
import FormData from "form-data";

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrapF"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 400,
    fontSize: 17
  },
  button: {
    margin: theme.spacing.unit
  },
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  }
});

class VerifyPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: this.props.user.userId,
      card: null
    };
  }

  uploadHandler = () => {
    console.log(this.state);
    const formData = new FormData();
    formData.append(`userId`, this.state.userId);
    formData.append(`card`, this.state.card);
    const config = {
      headers: {
        Authorization: "Bearer " + this.props.user.token
      }
    };
    if (this.state.card == null) {
      alert("Please add image.");
    } else {
      axios.post(`${process.env.REACT_APP_DEV_API_URL}/user/upload_card`, formData, config);
      alert("The image is successfully uploaded.");
    }
  };

  fileChangedHandler = event => {
    this.setState({ card: event.target.files[0] });
  };

  render() {
    const { classes } = this.props;
    return (
      <div style={{ display: "flex", justifyContent: "center" }}>
        <form
          className={classes.container}
          noValidate
          autoComplete="off"
          style={{ display: "inline" }}
        >
          <Paper className={classes.root} elevation={1}>
            <Grid
              container
              justify="center"
              alignItems="center"
              className={classes.textField}
            >
              <Typography variant="h5" component="h3">
                Verification
              </Typography>
            </Grid>
            <br />
            <Typography
              className={classes.textField}
              gutterBottom
              color="textSecondary"
              margin="normal"
            >
              Please upload your identity verification document to access the
              trading system on this website.
            </Typography>
            <br />
            <Grid container justify="center" alignItems="center">
              <input type="file" onChange={this.fileChangedHandler} />
              <button onClick={this.uploadHandler}>Upload!</button>
            </Grid>
            <br />
            <Grid container justify="center" alignItems="center">
              <Button
                variant="contained"
                size="small"
                className={classes.button}
                component={Link}
                to="/profile"
              >
                back
              </Button>
            </Grid>
          </Paper>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}

const connectedVerifyPage = connect(mapStateToProps)(
  withStyles(styles)(VerifyPage)
);
export { connectedVerifyPage as VerifyPage };
