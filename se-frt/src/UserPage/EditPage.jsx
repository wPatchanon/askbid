import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import classNames from "classnames";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import MenuItem from "@material-ui/core/MenuItem";
import CreditCardInput from "react-credit-card-input";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import axios from "axios";
import FormData from "form-data";

const styles = theme => ({
  avatar: {
    margin: 10,
    width: 80,
    height: 80
  },
  container: {
    display: "flex",
    padding: theme.spacing.unit * 2,
  },
  textField: {
    marginTop: 0,
    width: '100%',
  },
  menu: {
    width: 200
  },
  button: {
    margin: theme.spacing.unit
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  iconSmall: {
    fontSize: 20
  },
  root: {
    ...theme.mixins.gutters(),
    padding: theme.spacing.unit * 2,
  },
  fieldLabel: {
    marginTop: theme.spacing.unit
  }
});

const telNoRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
const fullNameRegex = /^([a-zA-Z0-9_-\sก-๛]){3,20}$/;
const addressRegex = /^([a-zA-Z0-9_-\s/.,ก-๛]){3,100}$/;

const costlist = [
  {
    label: "3000 Baht",
    value: "3000"
  },
  {
    label: "5000 Baht",
    value: "5000"
  },
  {
    label: "10000 Baht",
    value: "10000"
  },
  {
    label: "30000 Baht",
    value: "30000"
  }
];

class EditPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: "",
      address: "",
      telNo: "",
      oldFullName: "",
      oldAddress: "",
      oldTelNo: "",
      balance: "",
      bankNo: "",
      cost: "0",
      selectedFile: null,
      userId: this.props.user.userId
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  componentDidMount() {
    axios
      .post(
        `${process.env.REACT_APP_DEV_API_URL}/user/get_profile/`,
        { userId: this.props.user.userId },
        {
          headers: { Authorization: "Bearer " + this.props.user.token }
        }
      )
      .then(res => {
        this.setState(
          {
            fullName: res.data.result.full_name,
            address: res.data.result.address ? res.data.result.address : "",
            telNo: res.data.result.tel_no ? res.data.result.tel_no : "",
            balance: res.data.result.balance ? res.data.result.balance : "0",
            oldFullName: res.data.result.full_name,
            oldAddress: res.data.result.address,
            oldTelNo: res.data.result.tel_no
          },
          () => {
            console.log(res.data);
          }
        );
      });
  }

  handleChange(e) {
    this.setState({ [e.target.id]: e.target.value });
  }

  handleChange2 = name => event => {
    this.setState({ [name]: event.target.value });
  };

  onFormSubmit(e) {
    e.preventDefault();
    console.log(this.state.balance);
    if (
      this.state.fullName.match(fullNameRegex) &&
      this.state.address.match(addressRegex) &&
      this.state.telNo.match(telNoRegex)
    ) {
      let tmp = {
        fullName: this.state.fullName,
        address: this.state.address,
        telNo: this.state.telNo,
        balance: parseInt(this.state.balance) + parseInt(this.state.cost),
        bankNo: this.state.bankNo,
        userId: this.state.userId
      };
      if (tmp.fullName.trim() === " ") {
        tmp.fullName = this.state.oldFullName;
      }
      if (tmp.address.trim() === " ") {
        tmp.address = this.state.oldAddress;
      }
      if (tmp.telNo.trim() === " ") {
        tmp.telNo = this.state.oldTelNo;
      }
      const config = {
        headers: {
          Authorization: "Bearer " + this.props.user.token
        }
      };
      axios
        .post(`${process.env.REACT_APP_DEV_API_URL}/user/update_profile`, tmp, config)
        .then(res => {
          alert("The change was submitted.");
          this.props.history.push("/profile");
        });
    } else {
      alert("Invalid information");
    }
  }

  fileChangedHandler = event => {
    this.setState({ selectedFile: event.target.files[0] });
  };

  uploadHandler = () => {
    const formData = new FormData();
    formData.append(`userId`, this.state.userId);
    formData.append(`avatar`, this.state.selectedFile);
    const config = {
      headers: {
        Authorization: "Bearer " + this.props.user.token
      }
    };
    return axios
      .post(`${process.env.REACT_APP_DEV_API_URL}/user/upload_avatar`, formData, config)
      .then(res => {
        alert("The change was submitted.");
      });
  };

  state = { selectedFile: null };

  render() {
    const { classes } = this.props;
    return (
      <div style={{ display: "flex", justifyContent: "center" }}>
        <form
          className={classes.container}
          noValidate
          autoComplete="off"
          style={{ display: "inline" }}
        >
          <Paper className={classes.root} elevation={1}>
            <Grid container justify="center" alignItems="center">
              <Typography variant="h4">Edit your information</Typography>
            </Grid>
            <Typography variant="h6" className={classes.fieldLabel}>
              Avatar
            </Typography>
            <Grid container justify="center" alignItems="center">
              <input type="file" onChange={this.fileChangedHandler} />
              <Button
                variant="contained"
                color="primary"
                onClick={this.uploadHandler}
              >
                Upload!
              </Button>
            </Grid>
            <Typography variant="h6" className={classes.fieldLabel}>
              Full Name
            </Typography>
            <TextField
              error={!this.state.fullName.match(fullNameRegex)}
              label={this.state.fullName}
              id="fullName"
              className={classes.textField}
              onChange={this.handleChange.bind(this)}
              margin="normal"
            />
            <Typography variant="h6" className={classes.fieldLabel}>
              Address
            </Typography>
            <TextField
              error={!this.state.address.match(addressRegex)}
              label={this.state.address}
              id="address"
              className={classes.textField}
              onChange={this.handleChange.bind(this)}
              margin="normal"
            />
            <Typography variant="h6" className={classes.fieldLabel}>
              Tel No
            </Typography>
            <TextField
              error={!this.state.telNo.match(telNoRegex)}
              label={this.state.telNo}
              id="telNo"
              className={classes.textField}
              onChange={this.handleChange.bind(this)}
              margin="normal"
            />
          </Paper>
          <br />
          <br />
          <Paper className={classes.root} elevation={1}>
            <Grid container justify="center" alignItems="center">
              <Typography variant="h5" component="h3">
                Add Balance
              </Typography>
            </Grid>
            <br />
            <br />
            <CreditCardInput
              cardCVCInputRenderer={({ handleCardCVCChange, props }) => (
                <input
                  {...props}
                  onChange={handleCardCVCChange(e =>
                    console.log("cvc change", e)
                  )}
                />
              )}
              cardExpiryInputRenderer={({ handleCardExpiryChange, props }) => (
                <input
                  {...props}
                  onChange={handleCardExpiryChange(e =>
                    console.log("expiry change", e)
                  )}
                />
              )}
              cardNumberInputRenderer={({ handleCardNumberChange, props }) => (
                <input
                  {...props}
                  onChange={handleCardNumberChange(e =>
                    console.log("number change", e)
                  )}
                />
              )}
            />
            <br />
            <TextField
              select
              id="cost"
              label={"Select cost"}
              value={this.state.cost}
              className={classes.textField}
              onChange={this.handleChange2("cost")}
              SelectProps={{
                MenuProps: {
                  className: classes.menu
                }
              }}
              margin="normal"
            >
              {costlist.map(option => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </Paper>
          <br />
          <Grid container justify="center" alignItems="center">
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={this.onFormSubmit}
            >
              <SaveIcon
                className={classNames(classes.leftIcon, classes.iconSmall)}
              />
              save
            </Button>

            <Button
              variant="contained"
              color="secondary"
              className={classes.button}
              component={Link}
              to="/profile"
            >
              back
            </Button>
          </Grid>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}

const connectedEditPage = connect(mapStateToProps)(
  withStyles(styles)(EditPage)
);
export { connectedEditPage as EditPage };
