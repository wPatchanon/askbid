import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrapF"
  },
  textField: {
    width: '100%',
    fontSize: 17
  },
  button: {
    margin: theme.spacing.unit
  },
  root: {
    ...theme.mixins.gutters(),
    padding: theme.spacing.unit * 2,
  }
});

const costlist = [
  {
    label: "Problem 1",
    value: "1"
  },
  {
    label: "Problem 2",
    value: "2"
  },
  {
    label: "Problem 3",
    value: "3"
  },
  {
    label: "Problem 4",
    value: "4"
  }
];

class ReportPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      address: "",
      tel: "",
      cost: "",
      pictures: []
    };
    this.handleSubmit.bind(this);
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  handleSubmit() {
    alert("Your identity verification document was sent.");
  }

  render() {
    const { classes } = this.props;
    return (
      <div style={{ display: "flex", justifyContent: "center" }}>
        <form
          className={classes.container}
          noValidate
          autoComplete="off"
          style={{ display: "inline" }}
        >
          <Paper className={classes.root} elevation={1}>
            <Grid
              container
              justify="center"
              alignItems="center"
              className={classes.textField}
            >
              <Typography variant="h4">
                Report problem
              </Typography>
            </Grid>
            <br />
            <Typography
              className={classes.textField}
              gutterBottom
              color="textSecondary"
              margin="normal"
            >
              Please choose your problem
            </Typography>
            <TextField
              select
              label="Cause select"
              className={classes.textField}
              value={this.state.cost}
              onChange={this.handleChange("cost")}
              SelectProps={{
                MenuProps: {
                  className: classes.menu
                }
              }}
              margin="normal"
            >
              {costlist.map(option => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <Typography
              className={classes.textField}
              gutterBottom
              color="textSecondary"
              margin="normal"
            >
              Add more detail about your problem
            </Typography>
            <TextField
              style={{ margin: 8 }}
              placeholder="Fill your description"
              fullWidth
              margin="normal"
              variant="filled"
            />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <Typography
              className={classes.textField}
              gutterBottom
              color="textSecondary"
              margin="normal"
            >
              (Optional) Please upload additional evidence below
            </Typography>
            <br />
            <Grid container justify="center" alignItems="center">
              <input type="file" onChange={this.fileChangedHandler} />
              <Button variant="contained"
                color="primary" onClick={this.uploadHandler}>
                Upload!
                </Button>
            </Grid>
            <br />
            <br />
            <br />
            <br />
            <Grid container justify="center" alignItems="center">
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                onClick={this.handleSubmit}
              >
                submit
              </Button>
              <Button
                variant="contained"
                color="secondary"
                className={classes.button}
                component={Link}
                to="/profile"
              >
                back
              </Button>
            </Grid>
          </Paper>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

const connectedReportPage = connect(mapStateToProps)(
  withStyles(styles)(ReportPage)
);
export { connectedReportPage as ReportPage };
