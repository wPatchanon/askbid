import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux';
import { Typography } from '@material-ui/core';
import { withStyles } from "@material-ui/core/styles";
import VerifyList from "./VerifyList";
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { SnackbarProvider, withSnackbar } from 'notistack';
import axios from 'axios'


const styles = theme => ({
    root: {
        // margin: 'auto auto 100px auto',
        // width: '80%',
    },
    list: {
        marginTop: '30px',
    }
});

function VerifyPageApp(props) {
    const { classes, user } = props;
    const [verifyList, setList] = useState([]);
    const [expanded, setExpanded] = React.useState(null);

    useEffect(() => {
        axios.post(`${process.env.REACT_APP_DEV_API_URL}/admin/get_all_cards`, {
            "level": user.level
        },
            {
                headers: {
                    "Authorization": `Bearer ${user.token}`
                }
            })
            .then(res => res.data.result.slice(0, 20))
            .then(resdata => setList(resdata));
    }, [])

    function handleVerify(id) {
        const func = () => {
            props.enqueueSnackbar(`Verified ID# ${id}`, { variant: 'success' });
            setExpanded(false);
        }
        return func;
    }

    const handleChange = panel => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    const show = verifyList.map((item, index) => (
        <VerifyList key={item.id} user={item} handleClick={handleVerify(item.id)} panel={index}
            handleChange={handleChange(index)} expanded={expanded} />
    ));

    return (
        <div className={classes.root}>
            <Typography variant='h4'>Verify list</Typography>
            <div className={classes.list}>
                {show}
            </div>

        </div>
    );
}

const mapStateToProps = state => ({
    user: state.authentication.user
});

const App = withSnackbar(connect(mapStateToProps)(withStyles(styles)(VerifyPageApp)));

function VerifyPage(props) {
    const { classes } = props;
    return (
        <SnackbarProvider maxSnack={3}  >
            <App />
        </SnackbarProvider>
    );
}

export { VerifyPage }