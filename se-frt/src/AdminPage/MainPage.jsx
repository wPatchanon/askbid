import React, { useState } from 'react'
import { Typography } from '@material-ui/core';
import { withStyles } from "@material-ui/core/styles";


const styles = theme => ({
    root: {
        margin: '100px auto auto auto',
        width: '80%',
    },
});

function MainPage(props) {
    const { classes } = props;

    return (
        <div>
            <Typography variant='h4'>
                Manage Page
            </Typography>
        </div>
    );
}

const page = withStyles(styles)(MainPage);
export { page as MainPage }