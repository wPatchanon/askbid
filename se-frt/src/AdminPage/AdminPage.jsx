import React, { useState } from "react";
import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { VerifyPage } from "./VerifyPage";
import { MainPage } from "./MainPage";
import { AddItemPage } from "./AddItem";
import { AddItemPage2 } from "./AddItem2";
import { ManageUserPage } from "./ManageUserPage";
import { ManageItemPage } from "./ManageItemPage";
import { withRouter, Switch, Route } from "react-router-dom";
import { connect } from 'react-redux';
import Drawer from "./Drawer";

const styles = theme => ({
  // root: {
  //   margin: "100px auto auto auto",
  //   width: "100%"
  // },
  // main: {
  //   width: "80%",
  //   alignItems: "center"
  // },
  all: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    overflowX: 'auto',
  },
});

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

function AdminPage(props) {
  const { classes } = props;

  React.useEffect(() => {
    if (props.userauth.level !== 2)
      props.history.push({
        pathname: '/'
      })
  }, [])

  return (
    <div className={classes.all}>
      <Drawer />
      <TabContainer>
        <Switch>
          <Route exact path="/admin" component={MainPage} />
          <Route path="/admin/add_item" component={AddItemPage} />
          <Route path="/admin/add_item2" component={AddItemPage2} />
          <Route path="/admin/verify" component={VerifyPage} />
          <Route path="/admin/manage_user" component={ManageUserPage} />
          <Route path="/admin/manage_item" component={ManageItemPage} />
        </Switch>
      </TabContainer>
    </div>
  );
}

const mapStateToProps = state => ({
  userauth: state.authentication.user
});

const page = connect(mapStateToProps)(withStyles(styles)(withRouter(AdminPage)));
export { page as AdminPage };
