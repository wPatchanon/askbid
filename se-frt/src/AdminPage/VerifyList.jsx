import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import axios from 'axios'


const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: 16,
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: 14,
        color: 'secondary',
    },
    media: {
        width: '60%',
        margin: 'auto'
    }
}));

function ControlledExpansionPanels(props) {
    const classes = useStyles();
    const [imgData, setData] = React.useState();
    const { id, full_name, email, level, card_image } = props.user;
    const { userauth } = props;
    const [disable, setDisable] = React.useState(!(level === 0));
    const [color, setColor] = React.useState(level && '#b9f6ca')

    const fetchImg = () => {
        !imgData && axios.post(`${process.env.REACT_APP_DEV_API_URL}/admin/get_card_image`, {
            cardImage: card_image,
            level: 2,
        },
            {
                headers: {
                    "Authorization": `Bearer ${userauth.token}`
                }
            })
            .then(res => res.data)
            .then(resdata => {
                console.log(`fetch${id}`);
                setData(resdata.image64);
            })
    }

    function verify() {
        const data = {
            userId: id,
            level: 2,
        };
        const header = {
            "Authorization": `Bearer ${userauth.token}`
        }
        axios.post(`${process.env.REACT_APP_DEV_API_URL}/admin/verify`, data, { headers: header })
            .then(res => {
                console.log(res);
                setDisable(true);
                props.handleClick();
                setColor('#4caf50');
            }
            )
    }

    const handleChange = (event, isExpanded) => {
        isExpanded && fetchImg();
        props.handleChange(event, isExpanded);
    };

    return (
        <div className={classes.root}>

            <ExpansionPanel expanded={props.expanded === props.panel} onChange={handleChange}
                style={{ backgroundColor: color, marginTop: '10px', minWidth: 240 }}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>ID{id}     {full_name}</Typography>
                    <Typography className={classes.secondaryHeading}>{email}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div >
                        {imgData && <img src={`data:image/jpeg;base64,${imgData}`} className={classes.media} />}
                    </div>
                    <div>
                        <Button variant="contained" color="primary" onClick={verify} disabled={disable}>
                            {disable ? 'Verified' : 'Verify'}
                        </Button>
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>

        </div>
    );
}

const mapStateToProps = state => ({
    userauth: state.authentication.user
});

export default connect(mapStateToProps)(ControlledExpansionPanels);