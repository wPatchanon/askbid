import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import axios from "axios";

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrapF"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 400,
    fontSize: 17
  },
  button: {
    margin: theme.spacing.unit
  },
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  }
});

class AddItemPage2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      itemId: null,
      level: "2",
      image: null,
      showNext: false
    };
  }

  handleChange(e) {
    this.setState({ [e.target.id]: e.target.value });
  }

  fileChangedHandler = event => {
    this.setState({ image: event.target.files[0] });
  };

  uploadHandler = () => {
    const formData = new FormData();
    formData.append(`itemId`, this.props.location.data);
    formData.append(`image`, this.state.image);
    formData.append(`level`, this.state.level);
    const config = {
      headers: {
        Authorization: "Bearer " + this.props.user.token
      }
    };
    if (this.state.image === "") {
      alert("Please add image.");
    } else {
      axios
        .post(`${process.env.REACT_APP_DEV_API_URL}/admin/upload_item_image`, formData, config)
        .then(res => {
          this.setState({ showNext: true });
          alert("The change was submitted.");
        });
    }
  };

  render() {
    const { data } = this.props.location;
    const { classes } = this.props;
    return (
      <div>
        <Typography variant="h4">Add Item</Typography>
        <br />
        <Paper className={classes.root} elevation={1}>
          <Typography variant="h6">Upload Item Image</Typography>
          <br />
          <Grid container justify="center" alignItems="center">
            <input type="file" onChange={this.fileChangedHandler} />
            <button onClick={this.uploadHandler}>Upload!</button>
          </Grid>
          <br />
        </Paper>
        <br />
        <Grid container justify="center" alignItems="center">
          {this.state.showNext && (
            <Button
              variant="contained"
              size="small"
              className={classes.button}
              component={Link}
              to={{
                pathname: "/"
              }}
            >
              Submit
            </Button>
          )}
        </Grid>
        <br />
        <br />
        <br />
        <br />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}
const connectedAddItemPage2 = connect(mapStateToProps)(
  withStyles(styles)(AddItemPage2)
);

export { connectedAddItemPage2 as AddItemPage2 };
