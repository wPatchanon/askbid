import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import axios from "axios";

const styles = theme => ({
  container: {
    display: "flex",
  },
  textField: {
    width: '100%',
    fontSize: 17
  },
  button: {
    margin: theme.spacing.unit
  },
  root: {
    ...theme.mixins.gutters(),
    padding: theme.spacing.unit * 2,
  }
});

class ManageItemPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      itemId: "",
      level: "0",
      image: null,
      userId: this.props.user.userId
    };
  }

  handleChange(e) {
    this.setState({ [e.target.id]: e.target.value });
  }

  fileChangedHandler = event => {
    this.setState({ image: event.target.files[0] });
  };

  uploadHandler = () => {
    const formData = new FormData();
    formData.append(`itemId`, this.state.itemId);
    formData.append(`image`, this.state.image);
    formData.append(`level`, this.state.level);
    const config = {
      headers: {
        Authorization: "Bearer " + this.props.user.token
      }
    };
    return axios
      .post(`${process.env.REACT_APP_DEV_API_URL}/admin/upload_item_image`, formData, config)
      .then(res => {
        alert("The change was submitted.");
      });
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Typography variant="h4">Manage Item</Typography>
        <br />
        <Paper className={classes.root} elevation={1}>
          <br />
          <Typography variant="h6">
            Upload item image
          </Typography>
          <br />
          <TextField
            label={"ItemId"}
            id="itemId"
            className={classes.textField}
            onChange={this.handleChange.bind(this)}
            margin="normal"
          />
          <br />
          <br />
          <input type="file" onChange={this.fileChangedHandler} />
          <Button variant="contained" color="primary" onClick={this.uploadHandler}>Upload!</Button>
          <br />
          <br />
          <Grid container justify="center" alignItems="center">
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={this.onFormSubmit}
            >
              submit
            </Button>
          </Grid>
        </Paper>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}
const connectedManageItemPage = connect(mapStateToProps)(
  withStyles(styles)(ManageItemPage)
);

export { connectedManageItemPage as ManageItemPage };
