import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import { CLIENT_RENEG_LIMIT } from "tls";

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrapF"
  },
  textField: {
    width: '100%',
    fontSize: 17
  },
  button: {
    margin: theme.spacing.unit
  },
  root: {
    ...theme.mixins.gutters(),
    padding: theme.spacing.unit * 2,
  }
});

class AddItemPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      brand: "",
      desc: "",
      color: "",
      releasedDate: "",
      level: "0",
      selectedFile: null,
      userId: this.props.user.userId,
      itemId: null,
      showNext: false
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.id]: e.target.value });
  }

  onFormSubmit(e) {
    e.preventDefault();
    var date = new Date();
    let tmp = {
      brand: this.state.brand,
      desc: this.state.desc,
      color: this.state.color,
      releasedDate: date
        .toISOString()
        .replace(/T/, " ")
        .replace(/\..+/, ""),
      level: 2
    };
    const config = {
      headers: {
        Authorization: "Bearer " + this.props.user.token
      }
    };
    if (tmp.brand === "" || tmp.desc === "" || tmp.color === "") {
      alert("Please fill all field.");
    } else {
      axios
        .post(`${process.env.REACT_APP_DEV_API_URL}/admin/add_item`, tmp, config)
        .then(res => {
          this.setState({ itemId: res.data.itemId, showNext: true });
          alert("The item was added.");
        });
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Typography variant="h4">Add Item</Typography>
        <br />
        <Paper className={classes.root} elevation={1}>
          <Typography variant="h5">Fill Item Information</Typography>
          <br />
          <br />
          <Typography variant="h6">
            Item brand
          </Typography>
          <TextField
            label={"Example: Nike"}
            id="brand"
            className={classes.textField}
            onChange={this.handleChange.bind(this)}
            margin="normal"
          />
          <Typography variant="h6">
            Item description
          </Typography>
          <TextField
            label={"Example: Nike Air Force 1"}
            id="desc"
            className={classes.textField}
            onChange={this.handleChange.bind(this)}
            margin="normal"
          />
          <Typography variant="h6">
            Item color
          </Typography>
          <TextField
            label={"Example: Red"}
            id="color"
            className={classes.textField}
            onChange={this.handleChange.bind(this)}
            margin="normal"
          />
          <br />
          <br />
          <Button variant="contained" color="primary" onClick={this.onFormSubmit}>Add Item</Button>
          <br />
          <br />
        </Paper>
        <br />
        <Grid container justify="center" alignItems="center">
          {this.state.showNext && (
            <Button
              variant="contained"
              size="small"
              className={classes.button}
              component={Link}
              to={{
                pathname: "/admin/add_item2",
                data: this.state.itemId
              }}
            >
              Next
            </Button>
          )}
        </Grid>
        <br />
        <br />
        <br />
        <br />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}
const connectedAddItemPage = connect(mapStateToProps)(
  withStyles(styles)(AddItemPage)
);

export { connectedAddItemPage as AddItemPage };
