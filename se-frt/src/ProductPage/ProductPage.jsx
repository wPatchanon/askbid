import React, { useEffect, useState } from "react";
import ReactDOM from 'react-dom';

import axios from 'axios'
import Grid from '@material-ui/core/Grid';
import ImageGallery from 'react-image-gallery';
import { connect } from "react-redux";
import { red } from "@material-ui/core/colors";
import { withStyles, createStyles } from "@material-ui/core/styles";
import { Typography, Paper } from "@material-ui/core";
import "react-image-gallery/styles/css/image-gallery-no-icon.css";
import "typeface-roboto";
import Button from "@material-ui/core/Button";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import muitheme from "../theme/muitheme";
import OrderBar from "./OrderBar";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Divider from '@material-ui/core/Divider';
import Description from "./Description";
import OrderView from "./OrderView";
import { withRouter } from "react-router-dom";
import NumberFormat from 'react-number-format';


const styles = theme => ({
  root: {
    width: "80%",
    // margin: "80px auto 200px auto"
  },
  gallery: {
    width: 600,
    margin: "20px 20px 0px 0px"
  },
  desc: {
    margin: "20px auto auto 20px"
  },
  formControl: {
    // minWidth: 120,
  },
  showPrice: {
    // marginTop: 24,
    // marginBottom: 24,
  },
  paper: {
    marginTop: 24,
    minHeight: 100,
    padding: 20,
  },
  textBid: {
    color: '#00c853'
  },
  btn: {
    width: '100%'
  }
});

function ProductPage(props) {
  const { classes } = props;
  const [data, setData] = useState();
  const [size, setSize] = useState("None");
  const [open, setOpen] = useState(false)
  const [type, setType] = useState("")
  const [haveUpdate, setHaveUpdate] = useState(false)
  const id = props.match.params.id;
  const [level, setLevel] = useState()

  const sizeLabel = ['US4', 'US4.5', 'US5', 'US5.5', 'US6', 'US6.5', 'US7', 'US7.5', 'US8', 'US8.5', 'US9',
    'US9.5', 'US10', 'US10.5', 'US11', 'US12', 'US13', 'US14']

  const [resBid, setResBid] = useState([]);
  const [resAsk, setResAsk] = useState([]);

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_DEV_API_URL}/order/get_bid_price/${id}`)
      .then(res => {
        //console.log(res.data.result)
        setResBid(res.data.result)
      })
    axios.get(`${process.env.REACT_APP_DEV_API_URL}/order/get_ask_price/${id}`)
      .then(res => {
        //console.log(res.data.result)
        setResAsk(res.data.result)
      })
    setHaveUpdate(false)
  }, [size, haveUpdate])

  const reducerCount = (acc, cur) => {
    var price = cur.price
    var size = cur.size
    var count = acc[`${price}:${size}`] ? acc[`${price}:${size}`] + 1 : 1
    return {
      ...acc,
      [`${price}:${size}`]: count,
    }
  }
  const mapToarray = (key, val) => {
    const tmp = key.split(":")
    return (
      {
        price: tmp[0],
        amount: val,
        size: tmp[1],
      }
    )
  }

  let tempBid = resBid.filter(row => row.size === size || size === 'None').reduce(reducerCount, {})
  let bidCount = Object.keys(tempBid).map(key => mapToarray(key, tempBid[key]))
  let tempAsk = resAsk.filter(row => row.size === size || size === 'None').reduce(reducerCount, {})
  let askCount = Object.keys(tempAsk).map(key => mapToarray(key, tempAsk[key]))

  let lowestAsk = askCount.length ? askCount[0].price : 'N/A'
  let highestBid = bidCount.length ? bidCount[0].price : 'N/A'


  useEffect(() => {
    axios
      .get(`${process.env.REACT_APP_DEV_API_URL}/item/get_item/${id}`)
      .then(res => res.data.result)
      .then(resdata => setData(resdata));

    if (props.isLoggedIn) axios
      .post(
        `${process.env.REACT_APP_DEV_API_URL}/user/get_profile/`,
        { userId: props.userId },
        {
          headers: { Authorization: "Bearer " + props.token }
        }
      )
      .then(res => {
        setLevel(res.data.result.level);
      });
  }, []);

  const handleChange = event => {
    setSize(event.target.value);
  };

  const product = data ? data[0] : null;
  const image = data
    ? [
      {
        original: `${process.env.REACT_APP_DEV_API_URL}/images/${product.image}`,
        thumbnail: `${process.env.REACT_APP_DEV_API_URL}/images/${product.image}`
      }
    ]
    : null;

  function handleOpenAsk() {
    setOpen(true)
    setType(0)
  }
  function handleOpenBid() {
    setOpen(true)
    setType(1)
  }
  function handleClose() {
    setOpen(false)
  }
  function updatePage() {
    setHaveUpdate(true)
  }


  return data ? (
    <MuiThemeProvider theme={muitheme}>
      <div className={classes.root}>
        {props.isLoggedIn && <OrderBar detail={{ product, size, lowestAsk, highestBid }} update={updatePage} level={level} />}
        <Typography variant="h3"> {product.brand}</Typography>
        <Typography variant="h4"> {product.desc}</Typography>
        <Typography variant="h5"> {product.color}</Typography>
        <Grid container className={classes.detail} justify='center' alignItems="center">
          <Grid item className={classes.gallery}>
            <ImageGallery items={image} showBullets showThumbnails showNav />
          </Grid>

          <Grid item className={classes.desc}>


            <div className={classes.showPrice}>
              <Typography variant="h4" color="inherit">
                Lowest Ask
            </Typography>

              {lowestAsk === 'N/A' ? <Typography variant="h3" color="secondary">N/A</Typography> :
                <NumberFormat
                  thousandSeparator={true}
                  value={lowestAsk}
                  displayType={'text'}
                  renderText={value => <Typography variant="h3" color="secondary">{value}</Typography>}
                />
              }

              <Divider style={{ margin: "10px 0 10px 0" }} />
              <Typography variant="h4" color="inherit">
                Highest Bid
            </Typography>
              {highestBid === 'N/A' ? <Typography variant="h3" className={classes.textBid}>N/A</Typography> :
                <NumberFormat
                  thousandSeparator={true}
                  value={highestBid}
                  displayType={'text'}
                  renderText={value => <Typography variant="h3" className={classes.textBid}>{value}</Typography>}
                />
              }
            </div>
            <FormControl variant="outlined" className={classes.formControl} margin="normal">
              <InputLabel htmlFor="size">Size </InputLabel>
              <Select
                value={size}
                onChange={handleChange}
                input={
                  <OutlinedInput
                    labelWidth={32}
                    name="size"
                    id="size"
                  />
                }
              >
                <MenuItem value="None">
                  None
              </MenuItem>
                {sizeLabel.map(lab => <MenuItem value={lab} key={lab}>{lab}</MenuItem>)}
              </Select>
            </FormControl>
            <Grid container spacing={24}>
              <Grid item xs={24} sm={8}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.btn}
                  disableRipple
                  disableTouchRipple
                  size="large"
                  onClick={handleOpenAsk}
                >
                  View Ask
              </Button>
              </Grid>
              <Grid item xs={24} sm={8}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.btn}
                  disableRipple
                  disableTouchRipple
                  size="large"
                  onClick={handleOpenBid}
                >
                  View Bid
              </Button>
              </Grid>


            </Grid>
          </Grid>
        </Grid>

        <Description askCount={askCount} bidCount={bidCount} />
        <OrderView open={open} handleClose={handleClose} item={id} type={type} size={size} />

      </div>

    </MuiThemeProvider>
  ) : null;

}

const mapStateToProps = state => ({
  isLoggedIn: state.authentication.loggedIn,
  level: state.authentication.user.level,
  userId: state.authentication.user.userId,
  token: state.authentication.user.token,
});

const page = connect(mapStateToProps)(withStyles(styles)(withRouter(ProductPage)));
export { page as ProductPage };
