import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CreateAskStep from './CreateAskStep';
import SellStep from './SellStep';
import BuyStep from './BuyStep';
import CreateBidStep from './CreateBidStep';

function CreateOrderDialog(props) {
    let content
    if (props.dialog === 3)
        content = <CreateAskStep detail={props.detail} />
    else if (props.dialog === 2)
        content = <SellStep detail={props.detail} />
    else if (props.dialog === 1)
        content = <BuyStep detail={props.detail} />
    else if (props.dialog === 0)
        content = <CreateBidStep detail={props.detail} />

    return (
        <div>
            <Dialog open={props.open}
                onClose={props.handleClose}
                aria-labelledby="form-dialog-title"
                maxWidth="md"
                disableBackdropClick
                disableEscapeKeyDown
            >
                <DialogContent >
                    {content}
                </DialogContent>
                <DialogActions>
                    <Button onClick={props.handleClose} color="secondary">
                        Close & Exit
          </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default CreateOrderDialog;