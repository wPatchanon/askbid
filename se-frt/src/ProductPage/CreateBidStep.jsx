import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import axios from 'axios'
import NumberFormat from 'react-number-format';

const muiTheme = createMuiTheme({
    typography: {
        useNextVariants: true,
    },
    overrides: {
        MuiStepIcon: {
            root: {
                color: '#fce4ec', // or 'rgba(0, 0, 0, 1)'
                '&$active': {
                    color: '#f50057',
                },
                '&$completed': {
                    color: '#f50057',
                },
            },
        },
    }
});

const useStyles = makeStyles(theme => ({
    root: {
        minWidth: '400px',
    },
    button: {
        marginRight: "10px"
    },
    instructions: {
        marginTop: "10px",
        marginBottom: "10px",
    },
    underline: {
        '&:after': {
            borderBottom: "2px solid #FF3366"
        }
    },

}));


function getSteps() {
    return ['Set Bid Price', 'Select Address', 'Confirm Order'];
}




function CreateBidStep(props) {
    const classes = useStyles();
    const { userauth } = props;
    const [activeStep, setActiveStep] = React.useState(0);
    const [price, setPrice] = React.useState("");
    const [isLoading, setLoading] = React.useState(false);
    const steps = getSteps();
    const buttonLabel = ['Next', 'Next', 'Confirm'];
    const [addr, setAddr] = React.useState("None")
    const [fail, setFail] = React.useState(false);

    function getStepContent() {
        const handleChange = e => {
            var re = /^[1-9][0-9]*$/
            if (re.test(e.target.value) || e.target.value === '')
                setPrice(e.target.value)

        }
        switch (activeStep) {
            case 0:
                return (
                    <div style={{ marginLeft: 10 }}>
                        <Typography variant="h6">Input your Bid price in Baht.</Typography>
                        <br></br>
                        <FormControl fullWidth style={{ marginBottom: "20px" }}>
                            <InputLabel htmlFor="adornment-amount" >Price</InputLabel>
                            <Input
                                id="adornment-amount"
                                value={price}
                                onChange={handleChange}
                                startAdornment={<InputAdornment position="start">THB</InputAdornment>}
                                classes={{ underline: classes.underline }}
                            />
                        </FormControl>
                    </div>
                );
            case 1:
                return (
                    <div style={{ marginLeft: 10 }}>
                        <Typography variant="h6">The item will be sent to your address.</Typography>
                        <br></br>
                        <Typography variant="subtitle1">{addr}</Typography>
                        <br></br>
                    </div>
                )
            case 2:
                return (
                    <div style={{ marginLeft: 10 }}>
                        <Typography variant="subtitle1">Create Bid Order For</Typography>
                        <br></br>
                        <Typography variant="h6">Model: {props.detail.product.brand} {props.detail.product.desc}</Typography>
                        <Typography variant="h6">Size: {props.detail.size}</Typography>
                        <NumberFormat
                            value={price}
                            thousandSeparator
                            displayType={'text'}
                            renderText={value => <Typography variant="h6">Price: {value} Baht</Typography>}
                        />
                        <br></br>
                        <Typography variant="subtitle1">The item will be sent to address.</Typography>
                        <Typography variant="subtitle2">{addr}</Typography>
                        <br></br>
                    </div>
                )
            default:
                return 'Unknown step';
        }
    }

    const createBid = () => {
        setLoading(true);
        const data = {
            userId: userauth.userId,
            itemId: props.detail.product.id,
            size: props.detail.size,
            price: price,
            flag: 1,
            level: userauth.level
        };
        const header = {
            "Authorization": `Bearer ${userauth.token}`
        }
        axios.post(`${process.env.REACT_APP_DEV_API_URL}/order/create_order`, data, { headers: header })
            .then(res => {
                setLoading(false);
                if (res.data.status === 'fail') {
                    setFail(true)
                }
            }
            )
    }

    const getProfile = () => {
        const data = {
            userId: userauth.userId,
        };
        const header = {
            "Authorization": `Bearer ${userauth.token}`
        }
        axios.post(`${process.env.REACT_APP_DEV_API_URL}/user/get_profile`, data, { headers: header })
            .then(res => setAddr(res.data.result.address))
    }

    function handleNext() {
        setActiveStep(prevActiveStep => prevActiveStep + 1);
        if (activeStep === 2) createBid();
        else if (activeStep === 0) getProfile();
    }

    function handleBack() {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
    }

    function handleReset() {
        setActiveStep(0);
    }

    function checkValid(step) {
        if (step === 0) {
            return (price < 1000000) && (price !== '')
        }
        else
            return true
    }

    return (
        <div className={classes.root}>
            <MuiThemeProvider theme={muiTheme}>
                <Stepper activeStep={activeStep} >
                    {steps.map((label, index) => {
                        const stepProps = {};
                        const labelProps = { color: "secondary" };

                        return (

                            <Step key={label} {...stepProps}>
                                <StepLabel {...labelProps}
                                    style={{ color: "secondary" }}>{label}</StepLabel>
                            </Step>

                        );
                    })}
                </Stepper>
            </MuiThemeProvider>

            <div>
                {activeStep === steps.length ? (
                    <div style={{ marginLeft: 10 }}>
                        <Typography variant="h6" className={classes.instructions}>
                            {!isLoading ? fail ? "Your don't have enough balance." : "All steps completed - you're finished" : "Loading"}
                        </Typography>
                    </div>
                ) : (
                        <div>
                            {getStepContent(activeStep)}
                            <div>
                                <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
                                    Back
                                </Button>

                                <Button
                                    color="secondary"
                                    onClick={handleNext}
                                    className={classes.button}
                                    disabled={!checkValid(activeStep)}
                                >
                                    {buttonLabel[activeStep]}
                                </Button>
                            </div>
                        </div>
                    )}
            </div>
        </div>
    );
}

const mapStateToProps = state => ({
    userauth: state.authentication.user
});

export default connect(mapStateToProps)(CreateBidStep);