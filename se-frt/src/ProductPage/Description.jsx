import React, { useEffect, useState } from "react";
import ReactDOM from 'react-dom';

import { withStyles, createStyles } from "@material-ui/core/styles";
import { Typography, Paper } from "@material-ui/core";
import "react-image-gallery/styles/css/image-gallery-no-icon.css";
import "typeface-roboto";
import FiveTable from "./FiveTable";
import Grid from '@material-ui/core/Grid';


const styles = theme => ({
    root: {
    },

    paper: {
        // minWidth: 320,
        marginTop: 24,
        minHeight: 100,
        padding: 20,
        [theme.breakpoints.down("sm")]: {
            padding: 0,
        }
    },
    grid: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    table: {
        // minWidth: 300,
        margin: "auto",
        width: "45%",
    },
});

function Description(props) {
    const { classes } = props

    return (
        <div>

            <Paper className={classes.paper} square elevation={1}>
                <Grid container spacing={24} className={classes.grid} alignItems="center" justify="center">
                    <Grid item xs={12} sm={6} className={classes.table} >
                        <Typography variant="subtitle1">
                            5 Lowest Ask Price
                        </Typography>
                        <FiveTable type="Ask" row={props.askCount} />
                    </Grid>
                    <Grid item xs={12} sm={6} className={classes.table}>
                        <Typography variant="subtitle1">
                            5 Highest Bid Price
                        </Typography>
                        <FiveTable type="Bid" row={props.bidCount} />
                    </Grid>
                </Grid>
            </Paper>

        </div >
    )
}

export default withStyles(styles)(Description)