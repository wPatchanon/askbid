import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import NumberFormat from 'react-number-format';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: "auto",
        backgroundColor: '#fafafa',
    },
    table: {
        // minWidth: "300px",
        width: "100%",
        margin: "auto"
    },
    head: {
        color: "#000000",
    }
});

let id = 0;
function createData(name, calories, fat, carbs, protein) {
    id += 1;
    return { id, name, calories, fat, carbs, protein };
}



function FiveTable(props) {
    const { classes } = props;
    const rows = props.row.slice(0, 5)
    return (
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead >
                    <TableRow style={{ backgroundColor: props.type === 'Ask' ? "#ff80ab" : "#4db6ac" }}>
                        <TableCell align="left">
                            {props.type} Price
                        </TableCell>
                        <TableCell align="right">Size</TableCell>
                        <TableCell align="right">Amount</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row, idx) => (
                        <TableRow key={idx}>
                            <TableCell component="th" scope="row">
                                <NumberFormat
                                    thousandSeparator={true}
                                    value={row.price}
                                    displayType={'text'}
                                    renderText={value => <b>{value}</b>}
                                />

                            </TableCell>
                            <TableCell align="right">{row.size}</TableCell>
                            <TableCell align="right">{row.amount}</TableCell>
                        </TableRow>
                    ))}
                    {[0, 1, 2, 3, 4].map((row, idx) => {
                        if (idx < 5 - rows.length)
                            return (<TableRow key={idx}>
                                <TableCell component="th" scope="row" />
                                <TableCell align="right" />
                                <TableCell align="right" />
                            </TableRow>)
                    })}
                </TableBody>
            </Table>
        </Paper>
    );
}

export default withStyles(styles)(FiveTable);