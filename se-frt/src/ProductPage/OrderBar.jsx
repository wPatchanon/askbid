import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Button from "@material-ui/core/Button";
import CreateOrderDialog from "./CreateOrderDialog";
import { withRouter } from "react-router-dom";
import NumberFormat from 'react-number-format';


const styles = theme => ({
    appBar: {
        top: 'auto',
        bottom: '0',
        backgroundColor: '#dadada'
    },
    toolbarTop: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    toolbar: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 'auto'
    },
    fabButton: {
        position: 'absolute',
        zIndex: 1,
        top: -30,
        left: 0,
        right: 0,
        margin: '0 auto',
    },
    divider: {
        width: 1,
        height: 50,
        margin: 8,
    },
    buttonBuy: {
        margin: '10px',
        color: 'secondary',
        backgroundColor: 'secondary',
    },
    buttonSell: {
        margin: '10px',
        color: '#ffffff',
        backgroundColor: '#000000',
    },
    buttonSellOutlined: {
        margin: '10px',

    },
});

function OrderBar(props) {
    const { classes } = props;
    const [openDialog, setOpenDialog] = React.useState(false);
    const [dialog, setDialog] = React.useState();

    const checkSize = (size) => {
        return size === 'None'
    }

    function handleOpenBuy() {
        if (checkSize(props.detail.size)) {
            alert("Please Select Size")
            return
        }
        setOpenDialog(true);
        setDialog(1);
    }

    function handleOpenCreateOrder() {
        if (checkSize(props.detail.size)) {
            alert("Please Select Size")
            return
        }
        setOpenDialog(true);
        setDialog(3);
    }

    function handleOpenCreateBid() {
        if (checkSize(props.detail.size)) {
            alert("Please Select Size")
            return
        }
        setOpenDialog(true);
        setDialog(0);
    }

    function handleOpenSell() {
        if (checkSize(props.detail.size)) {
            alert("Please Select Size")
            return
        }
        setOpenDialog(true);
        setDialog(2);
    }

    function handleCloseDialog() {
        setOpenDialog(false);
        props.update();
    }

    return (
        <div>
            <AppBar position="fixed" color="primary" className={classes.appBar}>

                <Toolbar className={classes.toolbar}>
                    <Button
                        variant="outlined"
                        color="secondary"
                        className={classes.buttonBuy}
                        disableRipple
                        disableTouchRipple
                        size="large"
                        onClick={handleOpenCreateBid}
                    >
                        Create Bid
                </Button>
                    <Typography variant="subtitle1">OR</Typography>
                    <Button
                        variant="contained"
                        color="secondary"
                        className={classes.buttonBuy}
                        disableRipple
                        disableTouchRipple
                        size="large"
                        onClick={handleOpenBuy}
                        disabled={props.detail.lowestAsk === 'N/A'}
                    >
                        Buy Now {props.detail.lowestAsk === 'N/A' ? ' N/A ' : <NumberFormat
                            thousandSeparator={true}
                            value={props.detail.lowestAsk}
                            displayType={'text'}
                            renderText={value => ` ${value} `}
                        />
                        } Baht
                    </Button>
                    <Divider className={classes.divider} />
                    <Button
                        variant="contained"
                        className={classes.buttonSell}
                        disableRipple
                        disableTouchRipple
                        size="large"
                        onClick={handleOpenSell}
                        disabled={props.detail.highestBid === 'N/A' || props.level < 1}
                    >
                        Sell Now {props.detail.highestBid === 'N/A' ? ' N/A ' : <NumberFormat
                            thousandSeparator={true}
                            value={props.detail.highestBid}
                            displayType={'text'}
                            renderText={value => ` ${value} `}
                        />
                        }

                        Baht
                    </Button>
                    <Typography variant="subtitle1">OR</Typography>
                    <Button
                        variant="outlined"
                        color="inherit"
                        className={classes.buttonSellOutlined}
                        disableRipple
                        disableTouchRipple
                        size="large"
                        onClick={handleOpenCreateOrder}
                        disabled={props.level < 1}
                    >
                        Create Ask
                </Button>



                </Toolbar>
            </AppBar>

            <CreateOrderDialog open={openDialog} dialog={dialog} handleClose={handleCloseDialog} detail={props.detail} />

        </div>

    );
}

export default withStyles(styles)(withRouter(OrderBar));