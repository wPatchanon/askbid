import React from "react";
import { LoginPage } from "../LoginPage";
import { RegisterPage } from "../RegisterPage";
import Link from "@material-ui/core/Link";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  };
}

const styles = theme => ({
  paper: {
    position: "absolute",
    width: theme.spacing.unit * 70,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: "none",
    textAlign: "center"
  }
});

class UserLoginRegPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { RegBeDisabled: true, regOrLogin: "Register", open: false };
  }
  enableReg = () => {
    //this.props.dispatch(alertActions.clear());
    this.setState({
      RegBeDisabled: !this.state.RegBeDisabled,
      regOrLogin: this.state.RegBeDisabled ? "Login" : "Register"
    });
  };
  handleOpen = () => {
    this.setState({ open: !this.state.open });
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Button onClick={this.handleOpen}>reg</Button>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleOpen}
        >
          <div style={getModalStyle()} className={classes.paper}>
          <img disabled
              src={require("./logo.png")}
              style={{ width: 150, height: 150 }}
            />
            {this.state.RegBeDisabled ? <LoginPage /> : <RegisterPage />}
            <Link component="button" variant="body2" onClick={this.enableReg}>
              {this.state.regOrLogin}
            </Link>
          </div>
        </Modal>
      </div>
    );
  }
}

UserLoginRegPage.propTypes = {
  classes: PropTypes.object.isRequired
};

// We need an intermediary variable for handling the recursive nesting.
const UserLoginRegPages = withStyles(styles)(UserLoginRegPage);
export { UserLoginRegPages as UserLoginRegPage };

