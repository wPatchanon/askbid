import { userConstants } from "../_constants";

export function search(state = { searchKey: '', searchResult: [], isLoading: false }, action) {
    switch (action.type) {
        case userConstants.SET_KEYWORD:
            return {
                ...state,
                searchKey: action.keyword,
            }
        case userConstants.SEARCH_REQUEST:
            return {
                ...state,
                isLoading: true,
            };
        case userConstants.SEARCH_SUCESS:
            return {
                ...state,
                searchResult: action.result,
                isLoading: false,
            };
        default:
            return state;
    }
};