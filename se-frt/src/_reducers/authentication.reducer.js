import { userConstants } from '../_constants';

let user = JSON.parse(localStorage.getItem('user'));
const nullState = {
  loggedIn: false, user: {
    "status": null,
    "token": null,
    "userId": null,
    "level": null
  }
}
const initialState = user ? { loggedIn: true, user } : nullState;

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case userConstants.LOGIN_FAILURE:
      return nullState;
    case userConstants.LOGOUT:
      return nullState;
    default:
      return state
  }
}